const functions = require('firebase-functions');

const express = require('./src/express');

global.parseResult = function (result, res) {
	if (result.statusCode) {
		res.status(result.statusCode);
	}

	return res.send(result);
}


// Expose Express API as a single Cloud Function:
exports.api = functions.https.onRequest(express);

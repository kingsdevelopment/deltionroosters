const functions = require('firebase-functions');
const express = require('express');

const deltionConf = functions.config().deltion;

const needle = require('needle');
const moment = require('moment');

const router = express.Router();
router.use(require('../middleware/firebase'));


router.route('/:category/:week/:identifier').get((req, res) => {
	const week = req.params.week;
	const category = req.params.category;
	const identifier = req.params.identifier;

	let start = moment().day("Monday").week(week);
	let end = start.clone().add(7, 'days');

	start = start.format('YYYYMMDD');
	end = end.format('YYYYMMDD');

	const url = deltionConf.url + '/roster?' + category + '=' + identifier + '&start=' + start + '&end=' + end;

	return needle('GET', url)
		.then(result => {
			result = result.body;

			return res.send({
				data: result.data,
				remarks: result.remarks
			});
		});
});

module.exports = router;

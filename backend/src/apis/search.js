const functions = require('firebase-functions');
const express = require('express');

const algoliaConf = functions.config().algolia;

const algoliasearch = require('algoliasearch');

const needle = require('needle');
const _ = require('lodash');

const router = express.Router();
router.use(require('../middleware/firebase'));

router.route('/:q').get((req, res) => {
	if (!req.params.q) {
		const err = new Error("q is a required parameter");
		err.statusCode = 400;
		throw err;
	}

	const algoliaClient = algoliasearch(algoliaConf.id, algoliaConf.secret);
	const algoliaIndex = algoliaClient.initIndex('rosters');

	return algoliaIndex.search({
			query: req.params.q,
			hitsPerPage: 10
		})
		.then(result => res.send(result.hits));
});

module.exports = router;

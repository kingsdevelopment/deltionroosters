const functions = require('firebase-functions');
const express = require('express');

const deltionConf = functions.config().deltion;
const algoliaConf = functions.config().algolia;

const algoliasearch = require('algoliasearch');
const algoliaClient = algoliasearch(algoliaConf.id, algoliaConf.secret);
const algoliaIndex = algoliaClient.initIndex('rosters');

const needle = require('needle');
const _ = require('lodash');

const router = express.Router();
router.use(require('../middleware/secret'));

const deleteCategory = function (category) {
	const filters = {
		filters: 'category:' + category
	};
	return algoliaIndex.deleteBy(filters);
}

const getCategoryBatch = function (data, category) {
	const keys = Object.keys(data);

	return _.map(keys, key => {
		const entry = data[key];

		let toAdd = {
			action: 'addObject',
			indexName: 'rosters',
			body: {
				identifier: entry,
				displayName: entry,
				category: category
			}
		};

		if (category === 'teacher') {
			// teachers have different data structures.
			toAdd = {
				action: 'addObject',
				indexName: 'rosters',
				body: {
					identifier: key,
					displayName: entry[1],
					category: category
				}
			};
		}

		return toAdd;
	});
}

const addCategory = function (data, category) {
	return algoliaIndex.setSettings({
			'attributesForFaceting': ['filterOnly(category)']
		})
		.then(result => deleteCategory(category))
		.then(result => {
			const batchOperations = getCategoryBatch(data, category);
			return algoliaClient.batch(batchOperations);
		});
}

router.route('/').get((req, res) => {
	if (!req.query.category) {
		const err = new Error("category is a required parameter");
		err.statusCode = 400;
		throw err;
	}

	const categories = ['teacher', 'group', 'room'];
	const category = req.query.category;
	if (categories.indexOf(category) === -1) {
		const err = new Error("category can only be " + category.join(', '));
		err.statusCode = 400;
		throw err;
	}

	return needle('GET', deltionConf.url + '/' + category)
		.then(result => addCategory(result.body.data, category))
		.then(result => res.send({ status: 'ok', total: result }));
});

router.route('/all').get((req, res) => {

	const categories = ['teacher', 'group', 'room'];
	const data = _.map(categories, category => {
		return needle('GET', deltionConf.url + '/' + category)
			.then(result => {
				return {
					data: result.body.data,
					category: category
				}
			});
	});

	Promise.all(data)
		.then(results => Promise.all(_.map(results, data => addCategory(data.data, data.category))))
		.then(results => res.send({ status: 'ok', total: _.flatten(_.map(results, result => result.objectIDs)).length }))
		.catch(e => res.send(e));
});

module.exports = router;

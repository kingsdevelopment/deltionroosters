const express = require('express');
const cors = require('cors');

const app = express();
app.use(cors({ origin: true }));

app.use('/sync', require('./apis/sync'));
app.use('/search', require('./apis/search'));
app.use('/roster', require('./apis/roster'));

module.exports = app;

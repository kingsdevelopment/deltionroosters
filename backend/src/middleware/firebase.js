const admin = require('firebase-admin');
admin.initializeApp();

module.exports = function (req, res, next) {
	const authorization = req.header('Authorization');
	if (authorization) {
		let token = authorization.split(' ');
		admin.auth().verifyIdToken(token[1])
			.then((decodedToken) => {
				res.locals.user = decodedToken;
				next();
				return;
			})
			.catch(err => {
				console.log(err);
				res.sendStatus(401);
				return;
			});
	} else {
		console.log('Authorization header is not found');
		res.sendStatus(401);
		return;
	}
}

const functions = require('firebase-functions');

const middlewareConfig = functions.config().middleware;

module.exports = function (req, res, next) {
	const authorization = req.header('Authorization');
	if (authorization) {
		let token = authorization.split(' ');
		if (token[1] !== middlewareConfig.secret) {
			res.sendStatus(401);
			return;
		}

		next();
		return;
	} else {
		console.log('Authorization header is not found');
		res.sendStatus(401);
		return;
	}
}

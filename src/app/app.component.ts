import { Component } from '@angular/core';

import { Platform, ToastController } from '@ionic/angular';
import { Plugins, StatusBarStyle } from '@capacitor/core';

import { environment } from '../environments/environment';
import { AuthService } from './services/auth.service';

@Component({
	selector: 'dr-root',
	templateUrl: 'app.component.html'
})
export class AppComponent {
	public env = environment;

	private connectionUpdates = -1;

	constructor(private platform: Platform, private _auth: AuthService, private _toast: ToastController) {
		this.initializeApp();
	}

	initializeApp() {
		this.platform.ready().then(() => {
			if (this.platform.is('mobile')) {
				const { StatusBar } = Plugins;
				StatusBar.setStyle({
					style: StatusBarStyle.Light
				});

				const { SplashScreen } = Plugins;
				SplashScreen.hide();
			}
		});

		this._auth.connected
			.subscribe(value => {
				this.connectionUpdates++;
				this.showToast(value);
			});
	}

	async showToast(connected) {
		if (this.connectionUpdates > 1) {
			let message = 'Je bent verbonden met het internet.';
			if (!connected) {
				message = 'Je verbinding met het internet is verdwenen.';
			}

			const toast = await this._toast.create({
				message: message,
				duration: 2000
			});
			toast.present();
		}
	}
}

import { NgModule, LOCALE_ID } from '@angular/core';

import { registerLocaleData } from '@angular/common';
import localeNl from '@angular/common/locales/nl';
registerLocaleData(localeNl, 'nl');

import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, RouteReuseStrategy, Routes } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutesModule } from './app.routes';
import { HttpService } from './services/http.service';
import { AuthService } from './services/auth.service';
import { RosterService } from './services/rosters.service';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';

import { environment } from '../environments/environment';

@NgModule({
	declarations: [AppComponent],
	entryComponents: [],
	imports: [
		BrowserModule,
		HttpClientModule,

		AngularFireModule.initializeApp(environment.firebase),
		AngularFireAuthModule,

		IonicModule.forRoot(),

		AppRoutesModule
	],
	providers: [
		{ provide: LOCALE_ID, useValue: 'nl' },
		AuthService,
		HttpService,
		RosterService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }

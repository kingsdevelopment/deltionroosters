import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
	selector: 'dr-base',
	templateUrl: './base.component.html',
	styleUrls: ['./base.component.css']
})
export class BaseComponent implements OnInit {
	public activeTab = 'search';

	constructor(private _router: Router) { }

	ngOnInit() {
		this.setActiveTab(this._router.url);
		this._router.events
			.subscribe(value => {
				if (value instanceof NavigationEnd) {
					this.setActiveTab(value.url);
				}
			});
	}

	setActiveTab(url) {
		if (url === '/search') {
			this.activeTab = 'search';
		}
		if (url === '/roster') {
			this.activeTab = 'roster';
		}
		if (url === '/settings') {
			this.activeTab = 'settings';
		}
	}

	goTo($e) {
		if ($e.detail && $e.detail.value) {
			if (this.activeTab !== $e.detail.value) {
				this._router.navigateByUrl('/' + $e.detail.value);
			}
		}
	}

}

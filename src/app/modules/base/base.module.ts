import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseComponent } from './base.component';
import { BaseRoutesModule } from './base.routes';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../shared/shared.module';

@NgModule({
	imports: [
		CommonModule,
		IonicModule,
		BaseRoutesModule,
		SharedModule
	],
	declarations: [
		BaseComponent
	]
})
export class BaseModule { }

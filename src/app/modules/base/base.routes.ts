import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BaseComponent } from './base.component';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'roster'
	},
	{
		path: '',
		component: BaseComponent,
		children: [
			{
				path: 'search',
				loadChildren: '../../modules/search/search.module#SearchModule'
			},
			{
				path: 'roster',
				loadChildren: '../../modules/roster/roster.module#RosterModule'
			}]
	}
];

@NgModule({
	imports: [
		RouterModule.forChild(routes)
	]
})
export class BaseRoutesModule { }

import { Component, OnInit } from '@angular/core';
import { RosterService } from '../../services/rosters.service';
import { Router } from '@angular/router';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';

import * as moment from 'moment';
import * as _ from 'lodash';
import { AuthService } from '../../services/auth.service';

@Component({
	selector: 'dr-roster',
	templateUrl: './roster.component.html',
	styleUrls: ['./roster.component.scss'],
})
export class RosterComponent implements OnInit {
	public week;
	public weeks;
	public roster;
	public loading = false;
	public saved = false;

	public weekOptions = [];

	private connected = true;
	private rosters;
	private initialized = false;

	constructor(private _loader: LoadingController, private _alert: AlertController, private _toast: ToastController,
		private _roster: RosterService, private _router: Router, private _auth: AuthService) { }

	async ngOnInit() {
		this.generateWeeks();

		const hasRoster = await this._roster.exists();
		if (!hasRoster) {
			this._router.navigateByUrl('/search');
			return;
		}

		this._auth.connected
			.subscribe(value => {
				this.connected = value;
				this.generateWeeks();
			});

		this._roster.roster
			.subscribe(value => {
				this.rosters = value;
				this.generateWeeks();
				if (!this.initialized) {
					this.getRoster(this.week);
				}
			});

		this._roster.loading
			.subscribe(value => {
				this.saved = false;
				this.loading = value;
			});
	}

	generateWeeks() {
		if (this.connected) {
			const currWeek = moment().week();
			this.week = String(currWeek);
			const weeks = [(currWeek - 1), currWeek, (currWeek + 1), (currWeek + 2)];
			this.weeks = _.map(weeks, week => String(week));
		} else {
			this.weeks = _.map(this.rosters, roster => String(roster.week));
			this.week = this.weeks[0];
		}
	}

	async saveRoster() {
		await this._roster.saveRoster(this.week);

		this.saved = true;
		const toast = await this._toast.create({
			message: 'Rooster is opgeslagen!.',
			duration: 2000
		});
		toast.present();
	}

	getStatusColor(item) {
		const now = moment();

		const start = moment(item.start);
		const end = moment(item.end);

		let color = 'tertiary';

		if (now.isAfter(end)) {
			color = 'medium';
		}

		if (now.isAfter(start) && now.isBefore(end)) {
			color = 'success';
		}

		return color;
	}

	getDateColor(date) {
		const now = moment();
		const startOfDay = now.clone().startOf('day');
		const endOfDay = now.clone().endOf('day');

		date = moment(date).hour(12);

		let color = 'tertiary';

		if (date.isBefore(startOfDay)) {
			color = 'medium';
		}

		if (date.isAfter(startOfDay) && date.isBefore(endOfDay)) {
			color = 'success';
		}

		return color;
	}

	async getRoster(week) {
		const loading = await this._loader.create({
			content: 'Rooster ophalen'
		});
		await loading.present();

		this.roster = await this._roster.getWeek(week);
		this.week = String(this.roster.week);
		this.initialized = true;

		await loading.dismiss();
	}

	async selectWeek($event) {
		if ($event.detail && $event.detail.value) {
			const week = $event.detail.value;

			if (week !== this.week) {
				await this.getRoster(week);
			}
		}
	}

	async showRemarks() {
		let message = '';

		const remarks = _.orderBy(this.roster.remarks, 'important', 'desc');
		for (let i = 0; i < remarks.length; i++) {
			const remark = remarks[i];
			message += '<b>' + remark.header + '</b><br>';
			message += remark.content + '<br>';

			if (i < remarks.length) {
				message += '<br><br>';
			}
		}

		const alert = await this._alert.create({
			header: 'Let op!',
			message: message,
			buttons: ['Verbergen']
		});

		await alert.present();
	}
}

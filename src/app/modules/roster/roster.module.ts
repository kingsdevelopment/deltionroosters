import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { RosterComponent } from './roster.component';

import { RosterRoutesModule } from './roster.routes';
import { SharedModule } from '../shared/shared.module';

@NgModule({
	imports: [
		CommonModule,
		IonicModule,

		RosterRoutesModule,

		SharedModule
	],
	declarations: [RosterComponent]
})
export class RosterModule { }

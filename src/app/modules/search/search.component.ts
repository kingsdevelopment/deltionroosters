import { Component, OnInit } from '@angular/core';
import { RosterService } from '../../services/rosters.service';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { RosterModule } from '../roster/roster.module';
import { AuthService } from '../../services/auth.service';

@Component({
	selector: 'dr-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
	public searchResults: Array<any>;
	public searching = false;
	public connected = false;

	constructor(private _auth: AuthService, private _roster: RosterService, private _router: Router, private _loader: LoadingController) { }

	ngOnInit() {
		this._auth.connected
			.subscribe(value => {
				this.connected = value;
			});
	}

	getItems($e) {
		const value = $e.target.value;

		if (value) {
			this.searching = true;
			this.doSearch(value);
		} else {
			this.searching = false;
		}
	}

	private async doSearch(value) {
		const result = await this._roster.search(value.toUpperCase()).toPromise();
		this.searching = false;

		this.searchResults = result;
	}

	async openRoster(roster) {
		const loading = await this._loader.create({
			content: 'Rooster ophalen'
		});
		await loading.present();

		await this._roster.selectRoster(roster);

		await loading.dismiss();

		this._router.navigate(['/roster']);
	}
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { SearchComponent } from './search.component';
import { SearchRoutesModule } from './search.routes';
import { SharedModule } from '../shared/shared.module';

@NgModule({
	imports: [
		CommonModule,
		IonicModule,
		SearchRoutesModule,
		SharedModule
	],
	declarations: [SearchComponent]
})
export class SearchModule { }

import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

import { environment } from '../../environments/environment';

import { Plugins } from '@capacitor/core';

import { Platform } from '@ionic/angular';

@Injectable()
export class AuthService {
	private _authenticated = false;
	private _token;
	private _storageKey = 'auth';

	private _connected = false;

	private _tokenEmitter: EventEmitter<any> = new EventEmitter<any>();
	private _authenticatedEmitter: EventEmitter<any> = new EventEmitter<any>();
	private _connectedEmitter: EventEmitter<any> = new EventEmitter<any>();

	private _tokenObservable = Observable.create((observer: Observer<any>) => {
		observer.next(this._token);
		this._tokenEmitter.subscribe(a => observer.next(a));
	});

	private _authenticatedObservable = Observable.create((observer: Observer<any>) => {
		observer.next(this._authenticated);
		this._authenticatedEmitter.subscribe(a => observer.next(a));
	});

	private _connectedObservable = Observable.create((observer: Observer<any>) => {
		observer.next(this._connected);
		this._connectedEmitter.subscribe(a => observer.next(a));
	});

	constructor(private _auth: AngularFireAuth, private _platform: Platform) {
		this._auth.idToken
			.subscribe(token => {
				this.token = token;
			});

		this.createAnonymousToken();
	}

	get connected() {
		return this._connectedObservable;
	}

	set connected(value) {
		this._connected = value;
		this._connectedEmitter.emit(value);
	}

	get token() {
		return this._tokenObservable;
	}

	set token(value) {
		this._token = value;
		this.authenticated = value ? true : false;
		this._tokenEmitter.emit(this._token);
	}

	get authenticated() {
		return this._authenticatedObservable;
	}

	set authenticated(value) {
		this._authenticated = value;
		this._authenticatedEmitter.emit(this._authenticated);
	}

	async createAnonymousToken() {
		const { Network } = Plugins;

		try {
			if (this._platform.is('ios') || this._platform.is('android')) {
				const networkStatus = await Network.getStatus();
				this.connected = networkStatus.connected;

				Network.addListener('networkStatusChange', async (status) => {
					if (!this._connected && status.connected) {
						await this._auth.auth.signInAnonymously();
					}
					this.connected = status.connected;
				});
			} else {
				this.connected = true;
			}

			if (this._connected) {
				await this._auth.auth.signInAnonymously();
			}
		} catch (e) {
			this.connected = true;
			await this._auth.auth.signInAnonymously();
		}
	}
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { AuthService } from './auth.service';
import { Observable } from 'rxjs';


@Injectable()
export class HttpService {
	private _token;
	constructor(private _http: HttpClient, private _auth: AuthService) {
		this._auth.token.subscribe(token => this._token = token);
	}

	private getHeaders() {
		return new HttpHeaders().set('Authorization', 'Bearer ' + this._token);
	}

	private getRequest(url): Observable<any> {
		return this._http
			.get(url, {
				headers: this.getHeaders()
			})
			.pipe(map(res => res));
	}

	private postRequest(url, data = {}) {
		return this._http
			.post(url, data, {
				headers: this.getHeaders()
			})
			.pipe(map(res => res));
	}

	get(url): Observable<any> {
		return this.getRequest(url);
	}

	post(url, data = {}) {
		return this.postRequest(url, data);
	}
}

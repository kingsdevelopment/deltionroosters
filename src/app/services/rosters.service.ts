import { Injectable, EventEmitter } from '@angular/core';
import { HttpService } from './http.service';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';

import * as moment from 'moment';
import * as _ from 'lodash';
import { AuthService } from './auth.service';
import { Observer } from 'rxjs/Observer';

import { Plugins } from '@capacitor/core';

@Injectable()
export class RosterService {
	private _url = environment.firebase.functionsUrl;
	private _roster: any = {};
	private _rosterData: any;
	private _loading: any;
	private _connected = false;

	private _rosterEmitter: EventEmitter<any> = new EventEmitter<any>();

	private _rosterObservable = Observable.create((observer: Observer<any>) => {
		observer.next(this._roster);
		this._rosterEmitter.subscribe(a => observer.next(a));
	});

	private _loadingEmitter: EventEmitter<any> = new EventEmitter<any>();

	private _loadingObservable = Observable.create((observer: Observer<any>) => {
		observer.next(this._loading);
		this._loadingEmitter.subscribe(a => observer.next(a));
	});

	constructor(private _http: HttpService, private _auth: AuthService) {
		this.loadRoster();

		this._auth.connected
			.subscribe(value => {
				this._connected = value;
			});
	}

	search(q): Observable<Array<any>> {
		return this._http.get(this._url + '/search/' + q);
	}

	async getRoster(week) {
		this.loading = true;
		const url = this._url + '/roster/' + this._rosterData.category + '/' + week + '/' + this._rosterData.identifier;
		const result = await this._http.get(url).toPromise();
		return this.formatRoster(result, week, this._rosterData.identifier);
	}

	async exists() {
		const { Storage } = Plugins;
		const rosterData: any = await Storage.get({ key: 'roster' });

		let exists = false;
		if (rosterData.value) {
			exists = true;
		}

		if (!exists) {
			if (Object.keys(this._roster).length) {
				exists = true;
			}
		}

		return exists;
	}

	set roster(value) {
		if (value) {
			if (value.fromSave) {
				delete value.identifier;
				delete value.fromSave;
				this._roster = value;
			} else {
				value.roster = this._rosterData;

				if (this._rosterData.identifier !== value.identifier) {
					this._roster = {};
				}

				this._roster[value.week] = value;
			}

			this._rosterEmitter.emit(this._roster);
		}
	}

	get roster() {
		return this._rosterObservable;
	}

	set loading(value) {
		this._loading = value;
		this._loadingEmitter.emit(this._loading);
	}

	get loading() {
		return this._loadingObservable;
	}

	async getWeek(week) {
		if (this._roster[week]) {
			return this._roster[week];
		} else if (this._connected) {
			this.roster = await this.getRoster(week);
			return this._roster[week];
		} else {
			const keys = Object.keys(this._roster);
			return this._roster[keys[0]];
		}
	}

	async selectRoster(roster) {
		if (this._rosterData && roster.identifier !== this._rosterData.identifier) {
			this._roster = {};
		}

		this._rosterData = roster;
		const week = moment().week();
		this.roster = await this.getRoster(week);
	}

	private formatRoster(result, week, identifier) {
		const data = _.map(result.data, entry => {
			return {
				date: moment(entry.date).toDate(),
				items: this.formatItems(entry.items, entry.date)
			};
		});

		this.loading = false;
		return {
			data: data,
			remarks: result.remarks,
			week: week,
			identifier: identifier
		};
	}

	private formatItems(items, date) {
		return _.map(items, item => {
			const time = item.t.split(' - ');

			const startTime = time[0].split(':');
			const start = moment(date).hour(startTime[0]).minute(startTime[1]);

			const endTime = time[1].split(':');
			const end = moment(date).hour(endTime[0]).minute(endTime[1]);

			return {
				class: item.v,
				group: item.g,
				teacher: item.l,
				room: item.r,
				start: start.toDate(),
				end: end.toDate()
			};
		});
	}

	private async loadRoster() {
		const { Storage } = Plugins;

		let roster: any = await Storage.get({ key: 'roster' });
		if (roster && roster.value) {
			roster = JSON.parse(roster.value);

			delete roster.identifier;
			const keys = Object.keys(roster);
			if (keys.length) {
				this._rosterData = roster[keys[0]].roster;

				_.each(keys, key => {
					roster[key].fromSave = true;
				});

				roster.fromSave = true;
				this.roster = roster;
			}
		}
	}

	async saveRoster(week) {
		const { Storage } = Plugins;

		let roster: any = await Storage.get({ key: 'roster' });
		if (roster && roster.value) {
			roster = JSON.parse(roster.value);
			if (roster.identifier !== this._rosterData.identifier) {
				roster = {};
			}
		} else {
			roster = {};
		}

		roster[week] = this._roster[week];
		roster.identifier = this._rosterData.identifier;

		await Storage.set({
			key: 'roster',
			value: JSON.stringify(roster)
		});
	}
}

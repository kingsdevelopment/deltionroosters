export const environment = {
	production: true,
	firebase: {
		apiKey: 'AIzaSyBoyPJw2kkG7X5Y2uonVizN22Mjv1pRUJM',
		authDomain: 'deltionroosters.firebaseapp.com',
		databaseURL: 'https://deltionroosters.firebaseio.com',
		projectId: 'deltionroosters',
		storageBucket: 'deltionroosters.appspot.com',
		messagingSenderId: '424621608098',
		functionsUrl: 'https://us-central1-deltionroosters.cloudfunctions.net/api'
	}
};

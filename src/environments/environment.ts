export const environment = {
	production: false,
	firebase: {
		apiKey: 'AIzaSyBoyPJw2kkG7X5Y2uonVizN22Mjv1pRUJM',
		authDomain: 'deltionroosters.firebaseapp.com',
		databaseURL: 'https://deltionroosters.firebaseio.com',
		projectId: 'deltionroosters',
		storageBucket: 'deltionroosters.appspot.com',
		messagingSenderId: '424621608098',
		functionsUrl: 'http://localhost:5000/deltionroosters/us-central1/api'
	}
};
